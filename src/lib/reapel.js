import { createInterface } from './readline';
import { createContext, runInContext } from 'vm';
import { homedir } from 'os';
import { join } from 'path';
import jsTokens from 'js-tokens';
import { max, values } from 'lodash';
import { inspect } from 'util';
import { transform } from 'babel-core';
import stringMultiply from './util/string-multiply';

const { log, error } = console;

function Reapel(context = {}, opts = {}) {
  if (!(this instanceof Reapel)) return new Reapel(context, opts);
  if (typeof context !== 'object') throw TypeError('first argument to reapel must be an object');
  if (typeof opts !== 'object') throw TypeError('second argument to reapel must be an object if supplied');
  this._readline = createInterface({
    input: process.stdin,
    output: process.stdout,
    historyPath: join(homedir(), '.reapel_history')
  });
  this._prompt = '>> ';
  if (opts.prompt) this._prompt = opts.prompt;
  if (opts.globalStrict) {
    this._prefix = '"use strict"; void 0;';
  } else this._prefix = '';
  this._useBabel = opts.useBabel;
  this._readline.setPrompt(this._prompt);
  this._buffer = '';
  this._context = createContext(context);
  this._readline.on('line', (line) => {
    this._buffer += line;
    let indent = indented(this._buffer);
    if (!indent) {
      try {
        let retval = runInContext(xform.call(this, this._buffer), this._context);
        this._buffer = '';
        log(inspect(retval, { colors: true }));
      } catch (e) {
        error(e.stack);
        this._buffer = '';
      }
      if (this._lastIndent) this._readline.setPrompt(this._prompt);
    } else {
      this._readline.setPrompt(stringMultiply('.', this._prompt.length - 1) + stringMultiply('..', indent) + ' ');
    }
    this._lastIndent = indent;
    this._readline.prompt();
  });
  this._readline.prompt();
}

function xform (code) {
  if (this._prefix) code = this._prefix + code;
  if (this._useBabel) code = transform(code, { presets: ['es2015'] }).code;
  return code;
}

Reapel.prototype = {
  close() {
    this._readline.close();
  }
};
    
function indented(str) {
  let parts = str.match(jsTokens);
  let punc = parts.reduce((r, v) => {
    switch (v) {
      case '{':
        r.braces++;
        break;
      case '}':
        r.braces--;
        break;
      case '[':
        r.brackets++;
        break;
      case ']':
        r.brackets--;
        break;
      case '(':
        r.parens++;
        break;
      case ')':
        r.parens--;
        break;
    }
    return r;
  }, {
    braces: 0,
    parens: 0,
    brackets: 0
  });
  return max(values(punc));
}   

module.exports = Reapel;
