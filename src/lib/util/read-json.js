"use strict";

module.exports = function readJSON(path) {
  let retval = {};
  require.extensions['.json'](retval, path);
  return retval.exports;
};
