"use strict";

let backedUp;

let newFn;

function replaceHandler(fn) {
  newFn = fn;
  backedUp = process._events.SIGINT;
  process._events.SIGINT = void 0;
  process.on('SIGINT', fn);
}

function restoreHandlers() {
  process._events.removeListener(newFn);
  process._events.SIGINT = backedUp;
}

module.exports = {
  replaceHandler,
  restoreHandlers
};
