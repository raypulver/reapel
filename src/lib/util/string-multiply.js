"use strict";

module.exports = function stringMultiply(str, n) {
  let retval = '';
  for (let i = 0; i < n; ++i) {
    retval += str;
  }
  return retval;
};
