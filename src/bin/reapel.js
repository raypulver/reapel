#!/usr/bin/env node

"use strict";

import reapel from '../lib/reapel';
import repl from 'repl';

let context = Object.assign(repl._builtinLibs.reduce((r, v) => {
  r[v] = require(v);
  return r;
}, {}), global);

reapel(context, { useBabel: true, globalStrict: true });
