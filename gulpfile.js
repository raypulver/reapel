"use strict";

const gulp = require('gulp'),
  path = require('path'),
  fs = require('fs'),
  _ = require('lodash'),
  gutil = require('gulp-util'),
  runSequence = require('run-sequence'),
  mocha = require('gulp-mocha'),
  babel = require('gulp-babel'),
  jshint = require('gulp-jshint'),
  spawnSync = require('child_process').spawnSync,
  clone = require('clone'),
  map = require('async').map;

let jshintConfig = {};

require.extensions['.json'](jshintConfig, './.jshintrc');

let jshintServerConfig = jshintConfig.exports;
let jshintClientConfig = clone(jshintServerConfig);

jshintClientConfig.predef = jshintClientConfig.predef.client;
jshintServerConfig.predef = jshintServerConfig.predef.server;

let packageJson = require('./package');

gulp.task('default', ['build']);

const reserved = ['test', 'start', 'default'];

const srcPath = 'src/**/*.js';

jshint.client = jshint.bind(null, jshintClientConfig);
jshint.server = jshint.bind(null, jshintServerConfig);

function buildTestTask(testFramework) {
  let task = `node ${path.join('node_modules', testFramework, 'bin', testFramework)}`;
  if (arguments.length > 1) {
    task += ' ';
    task += [].slice.call(arguments, 1).join(' ');
  }
  return task;
}

gulp.task('build:tasks', function () {
  packageJson.scripts = {};
  _.forOwn(gulp.tasks, function (value, key, obj) {
    if (~reserved.indexOf(key)) return;
    packageJson.scripts[key] = `node ${path.join('node_modules', 'gulp', 'bin', 'gulp')} ${key}`;
  });
  packageJson.scripts.test = buildTestTask('mocha');
  fs.writeFileSync('./package.json', JSON.stringify(packageJson, null, 1));
});

gulp.task('default', function (cb) {
  runSequence(['build', 'jshint', 'test'], cb);
});

gulp.task('watch', ['default'], function () {
  return gulp.watch([ assetPath, srcPath ], ['default']);
});

gulp.task('test', function () {
  return gulp.src('test/test.js', { read: false })
    .pipe(mocha({ reporter: 'nyan' }));
});

gulp.task('build', function () {
  gulp.src('src/**/*.js')
    .pipe(babel({ presets: ['es2015'] }))
    .pipe(gulp.dest('dist'));
});

gulp.task('jshint', ['jshint:server']);


gulp.task('jshint:server', function () {
  return gulp.src('./src/**/*.js')
    .pipe(jshint.server())
    .pipe(jshint.reporter('jshint-stylish'));
});
