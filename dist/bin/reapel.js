#!/usr/bin/env node


"use strict";

var _reapel = require('../lib/reapel');

var _reapel2 = _interopRequireDefault(_reapel);

var _repl = require('repl');

var _repl2 = _interopRequireDefault(_repl);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var context = Object.assign(_repl2.default._builtinLibs.reduce(function (r, v) {
  r[v] = require(v);
  return r;
}, {}), global);

(0, _reapel2.default)(context, { useBabel: true, globalStrict: true });