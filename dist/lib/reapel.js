'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var _readline = require('./readline');

var _vm = require('vm');

var _os = require('os');

var _path = require('path');

var _jsTokens = require('js-tokens');

var _jsTokens2 = _interopRequireDefault(_jsTokens);

var _lodash = require('lodash');

var _util = require('util');

var _babelCore = require('babel-core');

var _stringMultiply = require('./util/string-multiply');

var _stringMultiply2 = _interopRequireDefault(_stringMultiply);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _console = console;
var log = _console.log;
var error = _console.error;


function Reapel() {
  var _this = this;

  var context = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
  var opts = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

  if (!(this instanceof Reapel)) return new Reapel(context, opts);
  if ((typeof context === 'undefined' ? 'undefined' : _typeof(context)) !== 'object') throw TypeError('first argument to reapel must be an object');
  if ((typeof opts === 'undefined' ? 'undefined' : _typeof(opts)) !== 'object') throw TypeError('second argument to reapel must be an object if supplied');
  this._readline = (0, _readline.createInterface)({
    input: process.stdin,
    output: process.stdout,
    historyPath: (0, _path.join)((0, _os.homedir)(), '.reapel_history')
  });
  this._prompt = '>> ';
  if (opts.prompt) this._prompt = opts.prompt;
  if (opts.globalStrict) {
    this._prefix = '"use strict"; void 0;';
  } else this._prefix = '';
  this._useBabel = opts.useBabel;
  this._readline.setPrompt(this._prompt);
  this._buffer = '';
  this._context = (0, _vm.createContext)(context);
  this._readline.on('line', function (line) {
    _this._buffer += line;
    var indent = indented(_this._buffer);
    if (!indent) {
      try {
        var retval = (0, _vm.runInContext)(xform.call(_this, _this._buffer), _this._context);
        _this._buffer = '';
        log((0, _util.inspect)(retval, { colors: true }));
      } catch (e) {
        error(e.stack);
        _this._buffer = '';
      }
      if (_this._lastIndent) _this._readline.setPrompt(_this._prompt);
    } else {
      _this._readline.setPrompt((0, _stringMultiply2.default)('.', _this._prompt.length - 1) + (0, _stringMultiply2.default)('..', indent) + ' ');
    }
    _this._lastIndent = indent;
    _this._readline.prompt();
  });
  this._readline.prompt();
}

function xform(code) {
  if (this._prefix) code = this._prefix + code;
  if (this._useBabel) code = (0, _babelCore.transform)(code, { presets: ['es2015'] }).code;
  return code;
}

Reapel.prototype = {
  close: function close() {
    this._readline.close();
  }
};

function indented(str) {
  var parts = str.match(_jsTokens2.default);
  var punc = parts.reduce(function (r, v) {
    switch (v) {
      case '{':
        r.braces++;
        break;
      case '}':
        r.braces--;
        break;
      case '[':
        r.brackets++;
        break;
      case ']':
        r.brackets--;
        break;
      case '(':
        r.parens++;
        break;
      case ')':
        r.parens--;
        break;
    }
    return r;
  }, {
    braces: 0,
    parens: 0,
    brackets: 0
  });
  return (0, _lodash.max)((0, _lodash.values)(punc));
}

module.exports = Reapel;