"use strict";

module.exports = function stringMultiply(str, n) {
  var retval = '';
  for (var i = 0; i < n; ++i) {
    retval += str;
  }
  return retval;
};