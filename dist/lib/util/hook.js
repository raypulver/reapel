"use strict";

var backedUp = void 0;

var newFn = void 0;

function replaceHandler(fn) {
  newFn = fn;
  backedUp = process._events.SIGINT;
  process._events.SIGINT = void 0;
  process.on('SIGINT', fn);
}

function restoreHandlers() {
  process._events.removeListener(newFn);
  process._events.SIGINT = backedUp;
}

module.exports = {
  replaceHandler: replaceHandler,
  restoreHandlers: restoreHandlers
};