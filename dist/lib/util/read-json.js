"use strict";

module.exports = function readJSON(path) {
  var retval = {};
  require.extensions['.json'](retval, path);
  return retval.exports;
};